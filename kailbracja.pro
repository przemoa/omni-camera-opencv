TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

DESTDIR = build

SOURCES += main.cpp


#INCLUDEPATH += /usr/local/include
#INCLUDEPATH += /usr/local/include/opencv
INCLUDEPATH += /usr/local/include/opencv2
LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui
LIBS += -lopencv_imgproc -lopencv_videoio -lopencv_ccalib -lopencv_calib3d

