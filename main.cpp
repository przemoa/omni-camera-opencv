#include <opencv2/core.hpp>
#include "opencv2/opencv.hpp"
#include "opencv2/videoio/videoio_c.h"
#include "opencv2/ccalib/omnidir.hpp"

using namespace std;


// parametry kamery uzyskane w wyniku kalibracji
cv::Mat K, xi, D, idx;

void kalibrujZPlikow()
{
    const string imgDir = "/home/przemek/Desktop/kailbracja/img2/";
    const string imgBaseName = "frame";

    std::vector <vector<cv::Point3f>> objectPointsVec;
    std::vector <vector<cv::Point2f>> imagePointsVec;

    cv::Mat imgOrginal;

    // 0 -33 zdjęcia z nagrania Y2
    // 34 - 144 zdjęcia z nagrania Y1


    // frames number to use
    for (int i = 34; i < 134; i+=3)
    {
        int board_height = 5;
        int board_width = 8;
        float space = 10;   // spacing between squares in chessboard [cm]

        if (i >= 34 && i <= 144)
        {
            // 9x6 board
            board_height = 6;
            board_width = 9;
            space = 8;
        }
        else
        {
            // 8x5 board
            board_height = 5;
            board_width = 8;
            space = 10;
        }

        std::stringstream ss;
        ss << std::setfill('0') << std::setw(4);
        ss << i;
        std::string s = ss.str();
        std::cout << s << endl;

        const string imgPath = imgDir + imgBaseName + s + ".jpg";
        imgOrginal = imread(imgPath, cv::IMREAD_GRAYSCALE);
        assert(imgOrginal.data && "Brak zdjecia");


        cv::Size patternsize(board_width, board_height); //interior number of corners
        cv::Mat gray = imgOrginal; //source image
        vector<cv::Point2f> corners; //this will be filled by the detected corners

        const bool patternfound = findChessboardCorners(gray, patternsize, corners,
                    cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE
                    + cv::CALIB_CB_FAST_CHECK);

        if (patternfound)
        {
            // subpixel resolution
            cornerSubPix(gray, corners, cv::Size(11, 11), cv::Size(-1, -1),
                cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
        }

        if (patternfound)
        {
            vector<cv::Point3f> obj;
            for (int i = 0; i < board_height; i++)
                for (int j = 0; j < board_width; j++)
                    obj.push_back(cv::Point3f((float)j * space, (float)i * space, 0));

            objectPointsVec.push_back(obj);
            imagePointsVec.push_back(corners);
        }

        drawChessboardCorners(imgOrginal, patternsize, cv::Mat(corners), patternfound);

        imshow("przetwarzane obrazy", imgOrginal);
        cv::waitKey(10);
    }

    int flags = 0;
    cv::TermCriteria critia(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 200, 0.0001);
    std::vector<cv::Mat> rvecs, tvecs;
    double rms = cv::omnidir::calibrate(
        objectPointsVec, imagePointsVec, imgOrginal.size(), K, xi, D, rvecs, tvecs, flags, critia, idx);
}

void kalibrujZWideo(std::string path)
{
    std::vector <vector<cv::Point3f>> objectPointsVec;
    std::vector <vector<cv::Point2f>> imagePointsVec;

    cv::VideoCapture vid(path); /// open the video
    assert(vid.isOpened() && "kamera nie otwarta"); /// check if we succeeded

    cv::Mat imgColor;
    cv::Size imgSize;
    while (1)
    {
        int board_height = 5;
        int board_width = 8;
        float space = 10;   // spacing between squares in chessboard [cm]

        // 8x5 board
        board_height = 5;
        board_width = 8;
        space = 10;
        for (int i  = 0; i < 4; i++)
        {
            vid >> imgColor;
            if (!imgColor.data)
                break;
        }
        if (!imgColor.data)
            break;

        imgSize = imgColor.size();

        cv::Mat gray;
        cv::cvtColor(imgColor, gray, cv::COLOR_BGR2GRAY);

        cv::Size patternsize(board_width, board_height); //interior number of corners
        vector<cv::Point2f> corners; //this will be filled by the detected corners

        const bool patternfound = findChessboardCorners(gray, patternsize, corners,
                    cv::CALIB_CB_ADAPTIVE_THRESH + cv::CALIB_CB_NORMALIZE_IMAGE
                    + cv::CALIB_CB_FAST_CHECK);

        if (patternfound)
        {
            // subpixel resolution
            cornerSubPix(gray, corners, cv::Size(11, 11), cv::Size(-1, -1),
                cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));
        }

        if (patternfound)
        {
            vector<cv::Point3f> obj;
            for (int i = 0; i < board_height; i++)
                for (int j = 0; j < board_width; j++)
                    obj.push_back(cv::Point3f((float)j * space, (float)i * space, 0));

            objectPointsVec.push_back(obj);
            imagePointsVec.push_back(corners);
        }

        drawChessboardCorners(imgColor, patternsize, cv::Mat(corners), patternfound);

        imshow("przetwarzane obrazy", imgColor);
        cv::waitKey(10);
    }

    int flags = 0;
    cv::TermCriteria critia(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 200, 0.0001);
    std::vector<cv::Mat> rvecs, tvecs;
    double rms = cv::omnidir::calibrate(
        objectPointsVec, imagePointsVec, imgSize, K, xi, D, rvecs, tvecs, flags, critia, idx);
}

void wypiszParametry()
{
    // Wartosci po kalibracji
    //    cv::Mat K, xi, D, idx;
    std::cout << "K: " << K << std::endl << std::endl;
    std::cout << "xi: " << xi << std::endl << std::endl;
    std::cout << "D: " << D << std::endl << std::endl;
}

void undistortSingleImage()
{
    cv::Mat imgOrg =  imread("/home/przemek/Desktop/kailbracja/img2/frame0050.jpg", cv::IMREAD_COLOR);

    // availacble undistorts

    cv::Mat imgUndistortCylindrical;
    cv::omnidir::undistortImage(imgOrg, imgUndistortCylindrical, K, D, xi, cv::omnidir::RECTIFY_CYLINDRICAL);
    imshow("imgUndistortCylindrical", imgUndistortCylindrical);

//        cv::Mat imgUndistortPerspective;
//        cv::omnidir::undistortImage(imgOrg, imgUndistortPerspective, K, D, xi, cv::omnidir::RECTIFY_PERSPECTIVE);
//        imshow("imgUndistortPerspective", imgUndistortPerspective);

//        cv::Mat imgUndistortLongati;
//        cv::omnidir::undistortImage(imgOrg, imgUndistortLongati, K, D, xi, cv::omnidir::RECTIFY_LONGLATI);
//        imshow("imgUndistortLongati", imgUndistortLongati);

    cv::Mat imgUndistortStereographic;
    cv::omnidir::undistortImage(imgOrg, imgUndistortStereographic, K, D, xi, cv::omnidir::RECTIFY_STEREOGRAPHIC);
    imshow("imgUndistortStereographic", imgUndistortStereographic);
}

void undistortVideo(std::string path)
{
    cv::Mat frame;

    const int FRAME_WIDTH = 1280;
    const int FRAME_HEIGHT = 720;

    cv::VideoCapture cap(path); /// open the video
    assert(cap.isOpened() && "kamera nie otwarta"); /// check if we succeeded
    cap.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

    int key = cv::waitKey(25);
    while (key != 'q')
    {
        cap >> frame;
        if (!frame.data)
            break;
        imshow("kamerka",frame);
        cv::Mat img;
        cv::omnidir::undistortImage(frame, img, K, D, xi, cv::omnidir::RECTIFY_CYLINDRICAL);
        imshow("img", img);
        key = cv::waitKey(25);
    }
}

void undistortCamera(int cameraNumber = 1)
{
    cv::Mat frame;

    const int FRAME_WIDTH = 1280;
    const int FRAME_HEIGHT = 720;

    cv::VideoCapture cap(cameraNumber); /// open the video
    assert(cap.isOpened() && "kamera nie otwarta"); /// check if we succeeded
    cap.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    cap.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);

    int key = cv::waitKey(25);
    while (key != 'q')
    {
        cap >> frame;
        imshow("kamerka",frame);
        cv::Mat img;
        cv::omnidir::undistortImage(frame, img, K, D, xi, cv::omnidir::RECTIFY_CYLINDRICAL);
        imshow("img", img);
        key = cv::waitKey(25);
    }
}

int main()
{
    std::string videoPathToCalibrate = "/home/przemek/my_video-5.mkv";

    kalibrujZWideo(videoPathToCalibrate);
    //kalibrujZPlikow();

    undistortSingleImage();

    wypiszParametry();

    cv::waitKey(1000);

    //undistortCamera();
    std::string videoPathToUndistort = "/home/przemek/my_video-7.mkv";
    undistortVideo(videoPathToUndistort);


    cv::waitKey(0);
    return 0;
}
